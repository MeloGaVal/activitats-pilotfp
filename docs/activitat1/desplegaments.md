# Gestió de desplegaments

## Objectius

Introduïr al professorat la funcionalitat dels **desplegaments** d'escriptoris virtuals i les diferents accions i interaccions amb l'alumnat que permet.

## Continguts

- Creació de desplegament d'escriptoris pels alumnes.
- Mostrar o ocultar escriptoris del desplegament als alumnes.
- Afegir alumnes a un desplegament existent.
- Crear nou escriptori original del desplegament a un alumne.
- Interactuar simultàniament amb els escriptoris dels alumnes.
- Descarregar visors directes dels escriptoris del desplegament.
- Eliminar un desplegament.

## Metodologia

El curs es realitza a l’aula virtual pilot [https://pilotfp.gencat.isardvdi.com](https://pilotfp.gencat.isardvdi.com). La metodologia es basa en què l’alumne (professorat inscrit) és el centre de l’aprenentatge. Aquest aprenentatge es construirà a partir de la pràctica proposada. Assolint les competències relacionades amb les metodologies actives mitjançant el feedback.

## Avaluació

S'avaluarà al sistema que s'hagin creat els ítems descrits en cada apartat del desenvolupament.

## Desenvolupament

### Crear dos desplegaments

Partint de dos grups d'alumnes ja existents al sistema s'han de crear dos desplegaments, un per a cada grup [https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-crear-desplegaments](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-crear-desplegaments).

Els grups del curs tenen el format *aula_a01 -> aula_a32*, *aula_b01 -> aula_b32*.

1. Desplegament1:
    - Fer visible pels usuaris
    - Seleccionar una plantilla d'escriptori
    - Seleccionar grups i buscar un grup dels anteriorment indicats
    - Comprovar al missatge de confirmació quants escriptoris es crearan.

2. Desplegament2:
    - Fer invisible pels usuaris
    - Seleccionar una plantilla d'escriptori diferent de la del desplegament1
    - Seleccionar grups i buscar un altre grup dels anteriorment indicats
    - Comprovar al missatge de confirmació quants escriptoris es crearan.

Ara veurem dos desplegaments i podrem entrar i veure a cadascun d'ells els escriptoris de cada alumne a la taula.

### Mostrar/Ocultar

Per a dur a terme aquesta acció de [visibilitzar escriptoris d'un desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#fer-visibleinvisible), usarem un usuari alumne d'un d'aquests dos grups amb els quuals hem creat els desplegaments, obrim la seva sessió simultàniament a una altra pestanya d'incògnit al navegador (o fent ús d'aquest [connector pel firefox](https://addons.mozilla.org/ca/firefox/addon/multi-account-containers/)).

1. Comprovar que a la pestanya d'incògnit on tenim l'alumne només veu l'escriptori del *Desplegament1* (visible) i no l'escriptori del *Desplegament2* (ocult).
2. A la pestanya original del nostre usuari professor on gestionem els desplegaments, intercanviar la visibilitat dels desplegaments i comprovar a la pestanya d'incògnit de l'alumne que li desapareix l'escriptori del *Desplegament1* i li apareix l'escriptori del *Desplegament2*.

Aquesta funcionalitat permet tenir control de quins escriptoris veuen els alumnes en cada moment del curs.

### Afegir alumnes

En cas que vulguem modificar el desplegament perquè el grup sobre el qual estava desplegat s'hi ha afegit alumnat, podrem usar la funcionalitat de [recrear](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#recrear-els-escriptoris) per tal de crear el nou escriptori del desplegament a aquests nous alumnes.

Com som professors (rol avançat) no podrem modificar l'alumnat que hi ha a cada grup. Però podrem obtenir el mateix resultat si eliminem l'escriptori del desplegament d'un dels alumnes i recreem el desplegament.

1. Eliminar de la taula d'escriptoris des de desplegament l'escriptori d'un alumne.
2. Clicar l'acció global de recrear el desplegament i comprovar que es torna a crear un escriptori per l'alumne que faltava.

### Crear nou escriptori a alumne

En cas que l'escriptori virtual d'un alumne tingui problemes, podrem eliminar-lo i seguir el procediment per a recrear el desplegament que hem fet a l'apartat anterior. L'alumne tornarà a tenir un escriptori a partir de la plantilla de la qual es va crear el desplegament inicialment.


### Interactuar amb els alumnes

Una de les funcionalitats que permet una millor interacció simultània amb l'alumne és la del [videowall](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#videowall).

Mitjançant aquesta opció podrem visualitzar simultàniament els escriptoris dels alumnes del desplegament que estiguin iniciats. També podrem clicar sobre un d'aquests escriptoris i prendre el control simultani amb l'alumne.

1. A la pestanya oculta on som alumne, iniciar l'escriptori del desplegament, com alumne.
2. Obrir el visor al navegador com a alumne
3. Com a professor, a la pestanya on som professors, anar a la opció de *videowall* d'aquest desplegament.
4. Realitzar alguna acció com a alumne a l'escriptori virtual iniciat. Comprovar que es veuen els canvis visuals al videowall de professor.
5. Per a interactuar amb l'escriptori sent un professor, clicar sobre el visor de l'escriptori de l'alumne al videowall. Aquest es farà gran. Comprovar que les accions que fem com alumne o com a professor es repliquen al visor de l'altre usuari.

### Visors directes

Com a professors podem descarregar el fitxer [CSV de visors directes](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-descarregar-se-el-fitxer-de-visors-directes).

Això ens permetrà donar accés a aquests escriptoris sense necessitat que l'alumne es trobi validat en el sistema.

Un dels casos pràctics on s'està fent servir és amb grups d'usuaris creats per a fer proves avaluables. El professor crea el desplegament sobre aquest grup d'examen i en descarrega el fitxer d'enllaços a visor directe i en distribueix cadascun a un alumne.

Mentre el desplegament estigui ocult ningú podrà fer ús de l'enllaç directe. En el moment que mostrem el desplegament els enllaços passaran a estar actius i l'alumnat podrà accedir a fer l'examen.

NOTA: Quan el desplegament és visible i l'alumnat accedeix a l'enllaç de visor directe, si l'escriptori es troba aturat, s'inicia automàticament.

1. Descarregar el fitxer CSV de visors directes d'un dels desplegaments que estigui ocult.
2. Accedir a l'enllaç d'un dels escriptoris d'alumne i comprovar que ens indica que no està visible actualment.
3. Fer visible el desplegament i tornar a carregar l'enllaç i comprovar que ara ens deixa accedir al visor de l'escriptori i que aquest, si no estava iniciat, s'ha iniciat automàticament.

### Eliminar desplegament

Deixar un dels desplegaments ocult i esborrar el segon desplegament.

Aquesta acció d'[esborrar desplegament](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/#com-esborrar-un-desplegament) no és reversible. Una vegada esborrat un desplegament ja no podrem recuperar ni el desplegament ni els escriptoris del desplegament.

1. Des de la vista resum de desplegaments clicar al botó d'esborrar d'un dels desplegaments.










